
#include "Player.h"
#include "Country.h"
#include <vector>
#include <iostream>
using namespace std;

int Player::num_of_players=0;



Player::Player() {
	num_of_players++;
	playerID= num_of_players;
	countriesOwned = 0;
	
}

void Player::setNumOfPlayers(int i){
	num_of_players = i;
}

void Player::printCountries(){
	for (vector<Country>::iterator it = countryVector.begin(); it != countryVector.end(); ++it) {
		cout << (*it).getCountryName() <<" ";
	}
}

void Player::addCountry(Country addedCountry){

	Player::countryVector.push_back(addedCountry);
	Player::countriesOwned++;
}

int Player::getCountriesOwned(){
	return countriesOwned;
}

int Player::getPlayerID(){
	return playerID;
}



Player::~Player() {
}

