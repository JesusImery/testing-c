#include "driver.h"
#include "PhaseDriver.h"
#include <iostream>
#include "Player.h"
#include "Country.h"
#include <sstream>
#include <time.h>
using namespace std;

driver::driver()
{
}

int main(){
	cout << "Beginning game!!" << endl;

	PhaseDriver sampleGame = PhaseDriver();

	sampleGame.initializePlayers();

	//Some sample countries are generated in this example. This part would be later tied to map class.

	Country countryArray[12];

	Country canada = Country("Canada");

	countryArray[0] = canada;

	Country usa = Country("Usa");

	countryArray[1] = usa;

	Country mexico = Country("Mexico");

	countryArray[2] = mexico;

	Country cuba = Country("Cuba");

	countryArray[3] = cuba;


	Country brazil = Country("Brazil");

	countryArray[4] = brazil;

	Country ecuador = Country("Ecuador");

	countryArray[5] = ecuador;

	Country peru = Country("Peru");

	countryArray[6] = peru;

	Country algeria = Country("Algeria");

	countryArray[7] = algeria;

	Country sudan = Country("Sudan");

	countryArray[8] = sudan;

	Country ivoryCoast = Country("Ivory Coast");

	countryArray[9] = ivoryCoast;


	Country tanzania = Country("Tanzania");

	countryArray[10] = tanzania;

	Country libia = Country("Libia");

	countryArray[11] = libia;

	//End of sample countries for this assignment only.

	sampleGame.randomCountryAssign(countryArray, sizeof(countryArray) / sizeof(*countryArray));

	//This method should later use the randomCountryAssign class to internally assign countries to players. Since the map functinality  (part 1 of this assignment) has not yet been
	//merged with this section, for now a country array will be artificially created and passed to the PhaseDriver instance manually in the 
	//main function.

	cout << "\n\nGame state before turns happen.\n" << endl;

	sampleGame.printGameState();

	cout << endl <<endl;

	int i = 0;

	int j = 1;

	while (sampleGame.victoryCondition() == false){

		sampleGame.mainPhase(&sampleGame.getPlayerArray()[i]);

		//Forcing Player 1 to win so the program finishes. For future classes, the main phase would require functionality to add and remove countries from players after succesful battles.
		//To force player 1 to win dummy countries are added until he reaches the maximum amount. The driver assumes that he has conquered all the countries, but the countries
		//owned by the rest of the players remained untouched. In the future exceptions will be added to the victory condition to avoid this.

		if (i == 0) {

			stringstream dummyCountryName;

			dummyCountryName << "Dummy Country " << j++;

			Country dummyCountry = Country(dummyCountryName.str());

			cout << "Adding " << dummyCountryName.str() << " to player " << sampleGame.getPlayerArray()[0].getPlayerID() << " (assume he conquered some country)." << endl;

			sampleGame.getPlayerArray()[0].addCountry(dummyCountry);

		}

		if (i == sampleGame.getNumOfPlayers() - 1) i = -1;

		i++;

	}

	cout << "\nPlayer " << sampleGame.winningPlayer().getPlayerID() << " wins!" << endl;

	cout << "\nFinal state for the players:"<<endl;

	sampleGame.printGameState();

	cout << "\n\nEnd of game." << endl;

	system("PAUSE");

	return 0;
}

driver::~driver()
{
}
