/*
 * Country.h
 *Country has limited functionality for now, its just here to demonstrate some relevant interactions with the phase driver.
 *To be merged in the future with part 1's work.
 *
 */

#ifndef COUNTRY_H_
#define COUNTRY_H_
#include <string>
using namespace std;

class Country {

private:
	string countryName;

public:
	Country(string name);
	Country();
	string getCountryName();
	virtual ~Country();
};


#endif /* COUNTRY_H_ */
