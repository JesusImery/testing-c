/*
 * PhaseDriver.h
 *
 *  Created on: 01/02/2015
 *      Author: Jesus Imery
 */

#ifndef PHASEDRIVER_H_
#define PHASEDRIVER_H_
#include <array>
#include "Player.h"
using namespace std;

class PhaseDriver {

private:

	int static const max_players=6;
	Player *playerArray;		//Holds the players in the game.
	int num_of_players;
	int static max_num_countries; //This variable could later be attached to a map class rather than the driver. For now it is there to assume that a game is over when all countries are conquered.


public:
	PhaseDriver();
		void initializePlayers();
		void mainPhase(Player *player);
		void randomCountryAssign(Country *array, int size);
		void randomlyShuffle(Country *array, int size);
		bool victoryCondition();
		bool PhaseDriver::victoryCondition(Player player);
		Player* getPlayerArray();
		int getNumOfPlayers();
		Player PhaseDriver::winningPlayer();
		void printGameState();
		virtual ~PhaseDriver();

};

#endif /* PHASEDRIVER_H_ */
