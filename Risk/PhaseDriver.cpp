/*
 * PhaseDriver.cpp
 *
 */

#include "PhaseDriver.h"
#include <iostream>
#include "Player.h"
#include "Country.h"
#include <sstream>
#include <time.h>
using namespace std;

int PhaseDriver::max_num_countries = 12;		//This variable could later be attached to a map class rather than the driver. For now it is there to assume that a game is over when all countries are conquered.

PhaseDriver::PhaseDriver() {

	int num_of_players=0;

	playerArray= new Player[max_players];

	Player::setNumOfPlayers(0);
	
}

void PhaseDriver::initializePlayers(){

	cout << "How many players for this session? From 2 to 6 players." <<endl;
	cin >> PhaseDriver::num_of_players;

	while (PhaseDriver::num_of_players < 2 || PhaseDriver::num_of_players > 6)
	{
		cout << "You have entered an invalid input. Please enter a integer number between 2 and 6." << endl;
		int num_of_players;
		cin >> num_of_players;

	}

	for (int i=0; i<PhaseDriver::num_of_players; i++){

		Player newPlayer;

	    PhaseDriver::playerArray[i]=newPlayer;

	}

	//This method should later use the randomCountryAssign class to internally assign countries to players. Since the map functinality  (part 1 of this assignment) has not yet been
	//merged with this section, for now a country array will be artificially created and passed to the PhaseDriver instance manually in the 
	//main function.


}

void PhaseDriver::randomCountryAssign(Country *array, int size){

	randomlyShuffle(array, size);
	int index=0;

	for(int i=0; i<size; i++) 	
		{
			PhaseDriver::playerArray[index].addCountry(array[i]);

			if (index==PhaseDriver::num_of_players-1)
				index=0;
			else index++;
	}
}

//Takes an array of countries and randomly shuffles thei indexes.
void PhaseDriver::randomlyShuffle(Country *array, int size)
{

	srand(time(0));

	for (int i=size-1; i>0; i--)
	{
		int j= rand() % (i+1);
		Country temp = array[i];
		array[i]=array[j];
		array[j]=temp;
}

}

//Work in progress for next assignments.
void PhaseDriver::mainPhase(Player *player){

	Player mainPlayer = *player;

	cout << "Main phase for player " << mainPlayer.getPlayerID() << endl;

	//Empty shell for now.

	cout << "An reinforce phase happens." << endl;

	cout << "An attack phase happens" << endl;

	cout << "A fortification phase happens" << endl;

	cout << "Player " << mainPlayer.getPlayerID() << "'s turn ends." << endl;

	//Evaluates some victory condition.

	//payer.reinforce();

	//player.attack();

}


//Evaluates victory condition for the game state. Meant to be executed at the end of battle phases during main phase. Subject to change in the future.
bool PhaseDriver::victoryCondition(){

	bool cond=false;

	for (int i=0; i<PhaseDriver::num_of_players; i++){

		
		if (playerArray[i].getCountriesOwned() == max_num_countries) cond = true;
	}

	return cond;
}

//Overloaded method for evaluating victory condition to a specific player
bool PhaseDriver::victoryCondition(Player player){

	return player.getCountriesOwned() == max_num_countries;
}

Player* PhaseDriver::getPlayerArray(){

	return playerArray;
}

int PhaseDriver::getNumOfPlayers(){

	return num_of_players;
};

void PhaseDriver:: printGameState(){

	for (int i = 0; i < num_of_players; i++){

		cout << "\nPlayer " << playerArray[i].getPlayerID() << " has " << playerArray[i].getCountriesOwned() << " countries." << endl;

		playerArray[i].printCountries();

	}
}

//Returns winning player or a dummy player when an error occurs. 
Player PhaseDriver::winningPlayer(){

	if (victoryCondition() == false)
		cout << "Error, no winning player yet." << endl;
	else

		for (int i = 0; i<num_of_players; i++){


			if (playerArray[i].getCountriesOwned() == max_num_countries) return playerArray[i];
		}
	
	Player player= Player();
	return player;

}


PhaseDriver::~PhaseDriver() {
}


