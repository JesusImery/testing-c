/*
 * Player.h
 *	Players own countries when he/she places armies on them, or conquers. These functionalities have yet to by added. For this example,
 *	dummy countries are added to the game and then one player will be forced countries until the game ends.
 * 
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include <vector>
#include "Country.h"
using namespace std;

class Player {

	private:
	int playerID;
	vector <Country> countryVector;	//Vector data type was selected since it can 
	static int num_of_players;
	int countriesOwned;

public:
	Player();
	void addCountry(Country addedCountry);
	int getCountriesOwned();
	int getPlayerID();
	static void setNumOfPlayers(int i);
	void printCountries();
	virtual ~Player();
};

#endif /* PLAYER_H_ */
