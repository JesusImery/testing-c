#include "stdafx.h"
#include "Testing.h"
#include <array>
#include <vector>
#include <iostream>
using namespace std;


Testing::Testing()
{
}

int* arrayStuff(int *array, int size){

int* array2= new int[size];

std::vector<int> shuffleVector;
for (int i = 0; i<size; ++i) shuffleVector.push_back(i);

cout << "printing shuffle vector before shuffle" << endl;

std::vector<int>::iterator it;

for (it = shuffleVector.begin(); it != shuffleVector.end(); ++it) 
	std::cout << (*it) << '\n';



std::random_shuffle(shuffleVector.begin(), shuffleVector.end());

for (int i = 0; i < size; i++) 		{

	int index = shuffleVector[i];

	int value = array[index];

	array2[i] = array[shuffleVector[i]];

}

return array2;


}

int main(){

	int array[10];

	int value = 20;

	for (int i = 0; i <= 9; i++) array[i] = value++;

	cout << "Initial Array" << endl;

	for (int i = 0; i <= 9; i++) cout << array[i] << endl;

	cout << "Scrambled randomly array" << endl;

	int placeholder = sizeof(array);

	int *array2= arrayStuff(array, 10);

	for (int i = 0; i <= 9; i++) cout << array[i] << endl;
	
	system("pause");

	return 0;

}

Testing::~Testing()
{
}
